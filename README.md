# UA Bootstrap #

This repository is the UA's flavor of bootstrap, combining the new UA Brand with the goodness of Bootstrap 3.

## Versioning Scheme ##

Because this front end CSS framework is based on Bootstrap 3, tagged versions use a modified form of [SemVer](http://semver.org/).
All versioned releases will be tagged with a 4 point version.
The first number is the major version of bootstrap being used (3).
The following 3 numbers will be using the normal SemVer scheme (major.minor.patch).
